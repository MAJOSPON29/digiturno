<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','login')->name('login.home')->middleware('guest');

Route::post('login','Auth\LoginController@login')->name('login');

Route::get('/home', 'HomeController@index')->name('home');

//rutas usuarios
Route::get('/usuarios', 'UserController@index')->name('users.index');
Route::get('/usuarios/crear','UserController@create')->name('users.create');
Route::get('/usuarios/{user}/editar','UserController@edit')->name('users.edit');
Route::patch('/usuarios/{user}','UserController@update')->name('users.update');
Route::post('/usuarios','UserController@store')->name('users.store');
Route::delete('/usuarios/{user}','UserController@destroy')->name('users.destroy');

//rutas turnos
Route::get('/turnos', 'TurnController@index')->name('turns.index');
Route::post('/turnos','TurnController@store')->name('turns.store');
Route::get('/turnos/{turn}','TurnController@show')->name('turns.show');
Route::patch('/turnos/{turn}','TurnController@update')->name('turns.update');
Route::delete('/turnos/{turn}','TurnController@destroy')->name('turns.destroy');

//rutas operadores
Route::get('/agenda', 'OperatorController@index')->name('operators.index');
Route::get('/agenda/show','OperatorController@show')->name('operators.show');
Route::patch('/agenda/{turn}','OperatorController@update')->name('operators.update');

Auth::routes();

