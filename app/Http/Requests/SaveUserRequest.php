<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required',
            'role_id'=>'required|numeric',
            'password'=>'min:6|required_with:password-confirm|same:password-confirm',
            'password-confirm'=>'required|min:6',
            'name'=>'required',
            'lastname'=>'required',
            'email'=>'required',
            'phone'=>'required',
        ];
    }

    public function messages()
    {
        return [
            //'role_id.required' => 'El usuario necesita un rol'
        ];
    }
}
