<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Turn;
use Mail;
use App\Mail\NewTurn;
class TurnController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    return view('turns.index');
	}

	 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datosTurno=request()->except(['_token','_method']);
        $correo=DB::table('users')->select('email')->where('id','=',$datosTurno['user_id'])->get();
        Turn::insert($datosTurno);

        Mail::to($correo[0]->email)->send(new NewTurn(auth()->user(),$datosTurno['date'],$datosTurno['window']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = DB::table('users')->select('role_id')->where('id','=',$id)->get();
        $role_user;

        foreach ($role as $rol) {
            $role_user=$rol->role_id;
        }

        if($role_user==3)
        	$data['turnos']=DB::table('turns')->select('id','user_id AS title','date AS start')->where('user_id','=',$id)->get();
        else
        	$data['turnos']=DB::table('turns')->select('id','user_id AS title','date AS start')->get();

        return response()->json($data['turnos']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$turnos=Turn::findOrFail($id);
    	Turn::destroy($id);
    	return response()->json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
    	$datosTurno=request()->except(['_token','_method']);
    	$respuesta=DB::update('UPDATE turns SET date=? WHERE id= ?',[$datosTurno['date'],$id]);
    	return response()->json($respuesta);
    }

}
