<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveUserRequest;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')
        ->join('roles','roles.id','=','users.role_id')
        ->select('users.id','roles.description as role','users.name','users.lastname','users.email','users.phone')
        ->get();

        return view('users.index',compact("users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=DB::table('roles')->select('id','description')->get();

        return view('users.create',compact('roles'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request)
    {
        $request->validated();

        User::create([
            'id'        => $request['id'],
            'role_id'   => $request['role_id'],
            'password'  => Hash::make($request['password']),
            'name'      => $request['name'],
            'lastname'  => $request['lastname'],
            'email'     => $request['email'],
            'phone'     => $request['phone'],

        ]);
        return redirect()->route('users.index')->with('status','El usuario fue creado con exito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        $roles=DB::table('roles')->select('id','description')->get();

        return view('users.edit',compact("user","roles"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, SaveUserRequest $request)
    {
        $user=User::find($id);

        $user->update([
            'role_id'   => $request['role_id'],
            'password'  => Hash::make($request['password']),
            'name'      => $request['name'],
            'lastname'  => $request['lastname'],
            'email'     => $request['email'],
            'phone'     => $request['phone'],

        ]);

        return redirect()->route('users.index')->with('status','El usuario fue actualizado con exito');
        //return var_dump($request['password']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();

        return redirect()->route('users.index')->with('status','El usuario fue eliminado con exito');
    }

}
