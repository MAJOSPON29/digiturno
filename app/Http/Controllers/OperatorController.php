<?php

namespace App\Http\Controllers;
use DB;
use App\Operator;
use Illuminate\Http\Request;

class OperatorController extends Controller
{
 	public function index()
 	{
 	    return view('operators.index');
 	}

 	/**
 	 * Display the specified resource.
 	 *
 	 * @param  int  $id
 	 * @return \Illuminate\Http\Response
 	 */
 	public function show()
 	{
 	    $data['turnos']=DB::select("SELECT id,user_id as title,date as start,status,if(status='PROGRAMADO','',if(status='ATENDIDO','green','red')) AS backgroundColor FROM turns");
 	    return response()->json($data['turnos']);

 	}

 	/**
 	 * Update the specified resource in storage.
 	 *
 	 * @param  \Illuminate\Http\Request  $request
 	 * @param  int  $id
 	 * @return \Illuminate\Http\Response
 	 */
 	public function update(Request $request,$id)
 	{
 		$datosTurno=request()->except(['_token','_method']);
 		$respuesta=DB::update('UPDATE turns SET status= ? WHERE id= ?',[$datosTurno['status'],$id]);
 		return response()->json($respuesta);
 	}
}
