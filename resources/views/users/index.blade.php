@extends('layout')

@section('tittle','Usuarios')
@section('tittle_area','Usuarios')

@section('content')
	<div class="container mt-3">
		<div class="d-flex justify-content-between align-items-center mb-3">
			<h3 class="mb-0"> Listado de Usuarios</h3>
			@auth
				<a class="btn btn-primary" href="{{route('users.create')}}">Crear Usuario</a>
			@endauth
		</div>

		<div class="container">
            <div class="single-table">
                <div class="table-responsive shadow">
                    <table class="table text-center table-bordered bg-white">
                        <thead class="text-uppercase bg-primary">
                            <tr class="text-white">
                                <th scope="col">Cedula</th>
                                <th scope="col">Rol</th>
                                <th scope="col">Nombres</th>
                                <th scope="col">Apellidos</th>
                                <th scope="col">Email</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($users as $user)
	                            <tr>
	                                <th scope="row">{{$user->id}}</th>
	                                <td>{{$user->role}}</td>
	                                <td>{{$user->name}}</td>
	                                <td>{{$user->lastname}}</td>
	                                <td>{{$user->email}}</td>
	                                <td>{{$user->phone}}</td>
	                                <td>

                                        <form class="d-inline" name="delete-user" method="POST" action="{{route('users.destroy',$user->id)}}" >
                                            @csrf @method('DELETE')
                                            <button class="btn btn-primary btn-sm" onclick="return toSubmit()"><i class="ti-trash"></i></button>
                                            <script type="text/javascript">
                                                function toSubmit() {
                                                    //Ingresamos un mensaje a mostrar
                                                    var mensaje = confirm("¿Desea borrar el usuario seleccionado?");
                                                    //Detectamos si el usuario acepto el mensaje
                                                    if (mensaje) {
                                                        return true;
                                                    }
                                                    //Detectamos si el usuario denegó el mensaje
                                                    else {
                                                        return false;
                                                    }
                                                }
                                            </script>
                                        </form>

                                        <a class="btn btn-primary btn-sm ml-2" href="{{route('users.edit',$user->id)}}"><i class="fa fa-edit"></i></a>
                                    </td>
	                            </tr>
                            @empty
                            	<th colspan="7" scope="row">No hay registros</th>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
@endsection