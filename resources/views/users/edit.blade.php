@extends('layout')

@section('tittle','Editar usuario')
@section('tittle_area','Editar usuario')

@section('content')
    <div class="container">
        <div class="row mt-3">
            <div class="col-12 col-sm-10 col-lg-8 mx-auto">
            	@include('partials.validation-errors')
                <form class="bg-white py-3 px-4 shadow rounded" method="POST" action="{{route('users.update',$user->id)}}">
                    @csrf @method('PATCH')
                    <h1 class="display-4">Edición de usuario</h1>

                    <div class="form-group d-none">
                    	<label for="tittle">Cedula del usuario </label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="id"
                    		type="text"
                    		name="id"
                    		value="{{old('id',$user->id)}}"
                    	>
                    </div>

                    <div class="form-group">
                        <label for="role_id">Rol del usuario</label>
                        <select class="form-control custom-select border-0 bg-light shadow-sm" id="role_id" name="role_id">
                          @foreach($roles as $rol)
                          	<option value="{{$rol->id}}">{{$rol->id}} - {{$rol->description}}</option>
                          @endforeach
                        </select>
                      </div>

                    <div class="form-group">
                    	<label for="password">Contraseña</label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="password"
                    		type="password"
                    		name="password"
                    		value="{{old('password',$user->password)}}"
                    	>
                    </div>

                     <div class="form-group">
                    	<label for="password-confirm">Confirmar contraseña</label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="password-confirm"
                    		type="password"
                    		name="password-confirm"
                    		value="{{old('password-confirm',$user->password)}}"
                    	>
                    </div>

                    <div class="form-group">
                    	<label for="name">Nombre</label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="name"
                    		type="text"
                    		name="name"
                    		value="{{old('name',$user->name)}}"
                    	>
                    </div>

                    <div class="form-group">
                    	<label for="lastname">Apellido</label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="lastname"
                    		type="text"
                    		name="lastname"
                    		value="{{old('lastname',$user->lastname)}}"
                    	>
                    </div>

                    <div class="form-group">
                    	<label for="email">Email</label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="email"
                    		type="email"
                    		name="email"
                    		value="{{old('email',$user->email)}}"
                    	>
                    </div>

                    <div class="form-group">
                    	<label for="phone">telefono</label>
                    	<input class="form-control border-0 bg-light shadow-sm"
                    		id="phone"
                    		type="text"
                    		name="phone"
                    		value="{{old('phone',$user->phone)}}"
                    	>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" id="actualizarTurno">Actualizar</button>
                    <a class="btn btn-link btn-block" href="{{route('users.index')}}">Cancelar</a>

                </form>
            </div>
        </div>
    </div>
@endsection
