<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	<title>TURNO RESERVADO</title>
</head>
<body>
	<p>Ha solicitado una reserva de turno con la siguiente información</p>
	 <ul>
        <li><strong>Nombre:</strong> {{ $user->name }} {{ $user->lastname }}</li>
        <li><strong>Cedula:</strong> {{ $user->id }}</li>
        <li><strong>Fecha:</strong> {{ $date }}</li>
        <li><strong>Ventanilla N°:</strong> {{ $window }}</li>
    </ul>
</body>
</html>