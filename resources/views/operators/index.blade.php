@extends('layout')

@section('tittle','Turnos agendados')
@section('tittle_area','Turnos agendados')

@section('scripts')
	<link rel="stylesheet" href="/assets/fullcalendar/core/main.css">
	<link rel="stylesheet" href="/assets/fullcalendar/daygrid/main.css">
	<link rel="stylesheet" href="/assets/fullcalendar/timegrid/main.css">

	<script type="text/javascript" src="/assets/fullcalendar/core/main.js" defer></script>
	<script type="text/javascript" src="/assets/fullcalendar/daygrid/main.js" defer></script>
	<script type="text/javascript" src="/assets/fullcalendar/timegrid/main.js" defer></script>
	<script type="text/javascript" src="/assets/fullcalendar/interaction/main.js" defer></script>

	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function() {
		    //configuracion del calendario
		    var calendarEl = document.getElementById('calendar');

		    var calendar = new FullCalendar.Calendar(calendarEl, {
		      plugins: [ 'timeGrid' , 'dayGrid', 'interaction'],
		      defaultView: 'timeGridDay',
		      allDaySlot: false,
		      slotDuration:'00:30:00',
		      minTime:'08:00:00',
		      slotLabelInterval:'00:30:00',
		      hiddenDays: [ 0 ],
		      defaultTimedEventDuration:'00:30:00',
		      //eventBackgroundColor:'green',

		      slotLabelFormat:{
		          hour: 'numeric',
		          minute: '2-digit',
		          omitZeroMinute: false,
		          meridiem: 'short'
		      },

		      header:{
		        left:'prev,next today',
		        center:'title',
		        right:'timeGridDay,dayGridMonth'
		      },

		      titleFormat:{ year: 'numeric', month: 'short', day: 'numeric' },

		      buttonText:{
						  today:    'hoy',
						  month:    'mes',
						  week:     'semana',
						  day:      'día',
						  list:     'lista'
			  },

		      eventClick:function(info) {
		      	console.log(info.event);
		      	$('#id').val(info.event.id);
		      	$('#user_id').val(info.event.title);
		      	$('#status').val(info.event.extendedProps.status);
		      	$('#date').val(info.event.start.toISOString().substring(0,10));
		      	$("#date").prop('disabled', true);

		      	$('#hour').val(info.event.start.toString().substring(16,18));
		      	if($('#hour').val()==null)
		      		$('#hour').val('08')

		      	$('#min').val(info.event.start.toString().substring(19,21));
		      	if($('#min').val()==null)
		      		$('#min').val('00')
		      	$('#createModal').modal('show');
		      	$('#exampleModalLabel').html("Evento");
		      },

		      events:"{{route('operators.show')}}",

		    });

		    calendar.setOption('locale', 'es');
		    calendar.render();

		    $('#btn-actualizar').click(function(){
		    	newEvent=capturarDatos("PATCH");
		    	enviarInfo('/'+$('#id').val(),newEvent);
		    });

		    function capturarDatos(method) {
		    	nuevoEvento={
		    		user_id:$('#user_id').val(),
		    		date:$('#date').val()+" "+$('#hour').val()+":"+$('#min').val()+":00",
		    		status:$('#status').val(),
		    		'_token':$("input[name='_token']").val(),
		    		'_method':method
		    	}
		    	return nuevoEvento;
		    }

		   	function enviarInfo(accion,objTurno){
		   		$.ajax(
		   			{
		   				type:"POST",
		   				url:"{{url('/agenda')}}"+accion,
		   				data:objTurno,
		   				success:function(msg) {
		   					$('#createModal').modal('hide');
		   					calendar.refetchEvents();
		   				},
		   				error:function(){alert("Hay un error");}
		   			}
		   		);
		   	}
		});
	</script>
@endsection

@section('content')
	<div class="container mt-3">
		<div class="row">
			<div class="col"></div>
			<div class="col-sm-12 col-md-8">
				<div id="calendar"></div>
			</div>
			<div class="col"></div>
		</div>
	</div>

	<!-- Modal creacion de eventp-->
	<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Nuevo evento</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	@csrf
      		<input type="text" id="id" name="id" value="" hidden>
      		<div class="form-group">
      			<label for="user_id">Cedula del usuario </label>
      			<input class="form-control border-0 bg-light shadow-sm"
      				id="user_id"
      				type="text"
      				name="user_id"
      				value=""
      				disabled="true"
      			>
      		</div>
      		<div class="form-group">
      			<label for="date">Fecha</label>
      			<input class="form-control border-0 bg-light shadow-sm"
      				id="date"
      				type="text"
      			>
      		</div>
      		<div class="form-group">
      			<label for="">Hora</label>
      			<div class="row">
      				<div class="col-md-3">
		      			<select class="form-control custom-select border-0 bg-light shadow-sm" id="hour" disabled>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
						</select>
      				</div>
      				<div class="col-md-3">
		      			<select class="form-control custom-select border-0 bg-light shadow-sm mt-3 mt-md-0" id="min" disabled>
		      				<option value="00" selected>00</option>
		      				<option value="15">15</option>
		      				<option value="30">30</option>
		      				<option value="45">45</option>
						</select>
      				</div>
      				<div class="col-md-5">
		      			<select class="form-control custom-select border-0 bg-light shadow-sm mt-3 mt-md-0" id="status">
		      				<option value="PROGRAMADO" selected>PROGRAMADO</option>
		      				<option value="ATENDIDO">ATENDIDO</option>
		      				<option value="NO ATENDIDO">NO ATENDIDO</option>
						</select>
      				</div>
      			</div>

      		</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	        <button type="button" class="btn btn-warning" id="btn-actualizar">Actualizar</button>

	      </div>
	    </div>
	  </div>
	</div>
@endsection


